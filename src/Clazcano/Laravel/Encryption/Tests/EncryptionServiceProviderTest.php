<?php

namespace Clazcano\Laravel\Encryption\Tests;

use Illuminate\Foundation\Application;
use Clazcano\Laravel\Encryption\Encrypter;
use Clazcano\Laravel\Encryption\EncryptionServiceProvider;

/**
 * Class EncryptionServiceProvider
 *
 * @package Clazcano\Laravel\Encryption
 */
class EncryptionServiceProviderTest extends TestCase
{
    public function testProvider()
    {
        $application = new Application();

        $provider = new EncryptionServiceProvider($application);
        $provider->register();

        $this->assertTrue($application->bound('encrypter'));
        $this->assertTrue($application->isAlias(Encrypter::class));
    }
}
